# alternative-labels
### cardinality
many
### type
:db.type/string
### definition
Acronyms, abbreviations, spelling variants, and irregular plural/singular forms may be included among the alternative labels for a concept.
# definition
### cardinality
one
### type
:db.type/string
### definition
Text defining the concept, is used for disambiguation.
# deprecated
### cardinality
one
### type
:db.type/boolean
### definition
If a concept is deprecated
# driving-licence-code-2013
### cardinality
one
### type
:db.type/string
### definition
Driving licence code
# esco-uri
### cardinality
one
### type
:db.type/string
### definition
ESCO Concept Identifier (URI)
# eures-code-2014
### cardinality
one
### type
:db.type/string
### definition
EURES code
# eures-nace-code-2007
### cardinality
one
### type
:db.type/string
### definition
NACE code according to EURES naming convention
# glottolog-uri
### cardinality
one
### type
:db.type/string
### definition
URI referring to Glottolog language concept
# hidden-labels
### cardinality
many
### type
:db.type/string
### definition
A lexical label for a resource that should be hidden when generating visual displays of the resource, but should still be accessible to free text search operations.
# id
### cardinality
one
### type
:db.type/string
### definition
Unique identifier for concept types
# implicit-driving-licences
### cardinality
many
### type
:db.type/ref
### definition
List of 'lower' ranking driving licences included in the licence
# isco-code-08
### cardinality
one
### type
:db.type/string
### definition
ISCO-08 level 4
# iso-3166-1-alpha-2-2013
### cardinality
one
### type
:db.type/string
### definition
Country code 2 letter
# iso-3166-1-alpha-3-2013
### cardinality
one
### type
:db.type/string
### definition
Country code 3 letter
# iso-639-1-2002
### cardinality
one
### type
:db.type/string
### definition
2 letter language code
# iso-639-2-1998
### cardinality
one
### type
:db.type/string
### definition
3 letter language code
# iso-639-3-2007
### cardinality
one
### type
:db.type/string
### definition
3 letter language code
# iso-639-3-alpha-2-2007
### cardinality
one
### type
:db.type/string
### definition
2 letter language code
# iso-639-3-alpha-3-2007
### cardinality
one
### type
:db.type/string
### definition
3 letter language code
# iso-uri
### cardinality
one
### type
:db.type/string
### definition
Official ISO URI
# label-en
### cardinality
one
### type
:db.type/string
### definition
English label for concept types
# label-sv
### cardinality
one
### type
:db.type/string
### definition
Swedish label for concept types
# lau-2-code-2015
### cardinality
one
### type
:db.type/string
### definition
Swedish Municipality code
# national-nuts-level-3-code-2019
### cardinality
one
### type
:db.type/string
### definition
Swedish Län code
# no-esco-relation
### cardinality
one
### type
:db.type/boolean
### definition
If a concept cannot be mapped to esco
# nuts-level-3-code-2013
### cardinality
one
### type
:db.type/string
### definition
OLD NUTS level 3 code
# nuts-level-3-code-2021
### cardinality
one
### type
:db.type/string
### definition
NUTS-2021 level 3 code
# preferred-label
### cardinality
one
### type
:db.type/string
### definition
What we prefer to call the concept.
# quality-level
### cardinality
one
### type
:db.type/long
### definition
Quality level
# related
### cardinality
many
### type
:db.type/ref
### definition
related concepts
# replaced-by
### cardinality
many
### type
:db.type/ref
### definition
Refers to other concepts that is replacing this one
# short-description
### cardinality
one
### type
:db.type/string
### definition
A short explanatory description of the concept to be used in GUIs
# sni-level-code-2007
### cardinality
one
### type
:db.type/string
### definition
SNI 2007 level code
# sni-level-code-2007-official-structure
### cardinality
one
### type
:db.type/string
### definition
SNI 2007 level code Official structure
# sort-order
### cardinality
one
### type
:db.type/long
### definition
Value for display sort order in category
# ssyk-code-2012
### cardinality
one
### type
:db.type/string
### definition
SSYK-2012 type
# sun-education-field-code-2000
### cardinality
one
### type
:db.type/string
### definition
Old SUN education field code, either 1, 2 or 3 digits and a letter
# sun-education-field-code-2020
### cardinality
one
### type
:db.type/string
### definition
SUN education field code, either 1, 2 or 3 digits and a letter
# sun-education-level-code-2000
### cardinality
one
### type
:db.type/string
### definition
Old SUN education level code, either 1, 2 or 3 digits
# sun-education-level-code-2020
### cardinality
one
### type
:db.type/string
### definition
SUN education level code, either 1, 2 or 3 digits
# type
### cardinality
one
### type
:db.type/string
### definition
The concepts main type
# unemployment-fund-code-2017
### cardinality
one
### type
:db.type/string
### definition
Swedish unemployment fund code
# unemployment-type-code
### cardinality
one
### type
:db.type/string
### definition
Swedish unemployment codes
# wikidata-uri
### cardinality
one
### type
:db.type/string
### definition
URI referring to concepts in Wikidata
