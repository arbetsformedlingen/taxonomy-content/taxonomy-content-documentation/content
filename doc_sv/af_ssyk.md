# Af-SSYK

## Definition
Arbetsförmedlingens anpassning av SSYK-strukturens fjärde nivå.

## Beskrivning

Af-SSYK är en modifierad variant av den fjärde nivån av den officiella upplagan av SSYK-strukturen.

Modifieringarna består av sammanslagningar och borttag av koder. Berörda koder och hur de kan översättas mellan Af-SSYK och den officiella varianten av SSYK nivå 4 [kan hittas här](https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/mappings).

Samtliga chefskoder har påverkats på så vis att nivåindelningen från den officiella versionen av SSYK inte används i Af-SSYK.

Exempelvis blir koderna 1311 (IT-chefer nivå 1) och 1312 (IT-chefer nivå 2) sammanslagna till 1310 (IT-chefer). Samma mönster gäller för de andra chefskoderna.

De koder som avser gårdstillverkning och förtroendevalda, 7619 och 4430, används inte i Af-SSYK. Även om koderna kan beskriva sysselsättningar gjordes bedömningen att exkludera dem då yrkena inom dem inte är föremål för rekrytering.

## Syfte

Af-SSYK togs fram samtidigt som SSYK-2012. Anledningen var att vissa koder inte bedömdes aktuella i matchningssammanhang. Detta gäller exempelvis för samtliga chefskoder, där nivåindelningen inte används. 

## Uppdateringsförfarande

Af-SSYK uppdateras i samband med att den officiella SSYK-strukturen (i dagsläget SSYK-2012) uppdateras. Dessa uppdateringar sker väldigt sällan (senaste cykeln var 16 år).
