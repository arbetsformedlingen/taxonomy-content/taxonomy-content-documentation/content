# Arbetsbeskrivningar

## Definition

Begrepp som beskriver förhållanden på en arbetsplats eller ett arbetes karaktär.

## Beskrivning

Arbetsbeskrivningarna förvaltas av Arbetsförmedlingen. Beskrivningarna består av begrepp som inordnas under ett antal kategoriseringar. Begreppen är tänkt att användas bland annat för att beskriva arbetsplatser eller som komplement till en [yrkesbenämning](https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/-/blob/main/doc_sv/yrkesben%C3%A4mningar.md?ref_type=heads), exempelvis vid beskrivning av en tidigare anställning i ett CV. 

I möjligaste mån följer begreppen språk som redan definierats av betrodda källor, exempelvis genom lagtexter (se anställningsformer) eller av Arbetsmiljöverket (se arbetsplatsförhållanden).

## Anställningsvaraktighet

Anställningsvaraktighet, eller `employment-duration` avser längden på en utlyst anställning. Exempelvis [Tills vidare](http://data.jobtechdev.se/taxonomy/concept/a7uU_j21_mkL) och [3 månader - upp till 6 månader](http://data.jobtechdev.se/taxonomy/concept/Xj7x_7yZ_jEn) 

Anställningsvaraktighet förekommer huvudsakligen vid platsannonsering. 


## Arbetsplatsförhållanden

Arbetsplatsförhållanden, eller `work-place-environment` syftar till arbetsplatsspecifika beskrivningar avseende arbetsmiljö eller en arbetsplats förutsättningar. Exempelvis [om den innebär beredskapstjänst](http://data.jobtechdev.se/taxonomy/concept/9xbf_bv7_eDq) eller [höghöjdsarbete](http://data.jobtechdev.se/taxonomy/concept/5j91_mbw_ESR).


## Arbetstid

Arbetstid, eller `worktime-extent`, beskriver ett arbetes omfattning i tid, antingen [heltid](http://data.jobtechdev.se/taxonomy/concept/6YE1_gAC_R2G) eller [deltid](http://data.jobtechdev.se/taxonomy/concept/947z_JGS_Uk2).

Begreppen förekommer huvudsakligen vid platsannonsering.


## Löneform

Löneform, eller `wage-type`, avser vilken typ av lön som gäller för en anställning, exempelvis [rörlig ackords- eller provisionslön](http://data.jobtechdev.se/taxonomy/concept/vVtj_qm6_GQu).


## Anställningsform

Anställningsform, eller `employment-type`, inbegriper begrepp som definierats som anställningsformer i [Lagen om anställningsskydd (LAS)](https://www.riksdagen.se/sv/dokument-och-lagar/dokument/svensk-forfattningssamling/lag-198280-om-anstallningsskydd_sfs-1982-80/), exempelvis [tillsvidareanställning](http://data.jobtechdev.se/taxonomy/concept/kpPX_CNN_gDU).

Anställningsformerna förekommer huvudsakligen i samband med platsannonsering. 

## Annan sysselsättningsform

Annan sysselsättningsform, eller `self-employment-type`, avser begrepp som kan användas för att beskriva typer av sysselsättning (kopplat till arbete) som inte kan definieras anställningsformer. Exempelvis [franchisetagare](http://data.jobtechdev.se/taxonomy/concept/hyM3_zia_hjN).

## Tid i yrke

Tid i yrke, eller `occupation-experience-year`, beskriver en arbetssökandes erfarenhet (i tid) inom ett yrke. Exempelvis [2-4 års erfarenhet](http://data.jobtechdev.se/taxonomy/concept/RWA2_uu6_udD) eller [ingen erfarenhet](http://data.jobtechdev.se/taxonomy/concept/9myv_bKh_YHf).

Tid i yrke förekommer huvudsakligen i beskrivningar av en arbetssökandes meriter, exempelvis CV:n.


## Exempel på GrahphQL-queries
[Exemplen nedan kan testas i vårat GraphiQL-gränssnitt](https://taxonomy.api.jobtechdev.se/v1/taxonomy/graphiql).


## Hämta alla sysselsättningsformer

```graphql
query MyQuery {
  concepts(type: "employment-variety") {
    id
    preferred_label
    definition
    type
    narrower {
      id
      preferred_label
      definition
      type
    }
  }
}
```

