# Attribut

## Definition
Strukturerade datafält som kopplas till begrepp.

## Beskrivning
Attribut i Taxonomy är kopplade till begrepp och används för att beskriva och kategorisera dem. Vissa begreppstyper, som `preferred-label`, `id`, `type`, `definition` och `uri` förekommer för samtliga begrepp, medan andra är mer typspecifika. Exempelvis finns attributet `ssyk-code-2012` enbart kopplat till begrepp i [SSYK-strukturen](https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/-/blob/main/doc_sv/ssyk.md).


Förteckning, inklusive korta beskrivningar, över samtliga attribut i [Taxonomy hittas här](https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/-/tree/main/attributes). 

## SKOS-attribut
Ett antal av attributen i Taxonomy är hämtade från [SKOS](https://www.w3.org/TR/skos-reference/). 


| skos             | taxonomy           |
| ---------------- | ------------------ |
| preferred-label    | [skos:prefLabel](http://www.w3.org/2004/02/skos/core#prefLabel)   |
| alternative-labels    | [skos:altLabel](http://www.w3.org/2004/02/skos/core#altLabel) |
| hidden-labels |    [skos:hiddenLabel](http://www.w3.org/2004/02/skos/core#hiddenLabel)   |

## Alternativa benämningar
Om ett begrepp har attributet för alternativa benämningar, eller `alternative-labels`, innebär det att alla benämningar som listas tillsammans med attributet ska kunna användas med samma betydelse som begreppets `preferred-label`. 
