# ESCO-kompetenser

## Definition
En ESCO-kompetens är likställd med en [esco skill](https://esco.ec.europa.eu/en/classification/skill_main), med svensk `preferred-label`. 

Det är EU-kommissionen som står som ägare av begreppet. 

## Beskrivning
[ESCO](https://esco.ec.europa.eu/sv) är en EU-gemensam klassificering av yrken och kompetenser, motsvarande Jobtech Taxonomy. Begreppen i ESCO är översatta till samtliga EU:s officiella språk plus bland annat arabiska och utgör ett nav för interoperabilitet mellan en nationell och en (inom EU) internationell nivå. 

EU:s medlemsstater är genom [Eures-direktivet](https://eur-lex.europa.eu/eli/reg/2016/589/oj) bundna till att antingen skapa mappningar mellan ESCO och sina nationella yrkes- och kompetensstrukturer, eller att helt implementera ESCO. Det huvudsakliga syftet är att CV:n och platsannonser som skickas till Eures ska berikas med referenser (URI:er) till de begrepp i ESCO som mappningsrelationerna från yrkesbenämningar eller kompetensbegrepp från den nationella strukturen, som ingår i CV:t eller platsannonsen, pekar på. I Sverige valde vi att mappa [yrkesbenämningar](https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/-/blob/main/doc_sv/yrkesben%C3%A4mningar.md) och [kompetensbegrepp](https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/-/blob/main/doc_sv/kompetensbegrepp.md) till motsvarande begreppstyper i ESCO. Mappningarna mellan begreppsstrukturerna tillgängliggörs via [Jobtech Taxonomy](https://jobtechdev.se/sv/komponenter/jobtech-taxonomy). Medlemsstaters mappningstabeller publiceras på [ESCOs hemsida](https://esco.ec.europa.eu/sv/node/408).

Kompetenser i ESCO skiljer på begrepp som rör färdighet och begrepp som rör kunskap. [Indelningen av kompetenser i ESCO](https://esco.ec.europa.eu/sv/classification/skill_main) bygger delvis på delar av den befintliga klassificeringen i O*Net och den kanadensiska färdighets- och kunskapsordlistan. ESCO-kompetenser är strukturerade enligt ’nödvändiga’ respektive ’kompletterande’ för enskilda yrken i ESCO. En kompetens i ESCO är kopplade enligt nödvändiga respektive kompletterande för ett eller flera yrken i ESCO. ESCOs färdigheter/kompetenser/kunskaper benämns hädanefter ”ESCO-kompetens”.

Arbetsförmedlingens och ESCOs klassifikationer skiljer sig väsentligt åt i vilken detaljnivå som tillämpas. ESCO har betydligt fler och mer detaljerade begrepp, till skillnad från Arbetsförmedlingen som har färre och bredare begrepp. Detta gäller både för kompetensbegrepp och yrkesbenämningar. 

Begrepp i ESCO identifieras med hjälp av beständiga URI:er, exempelvis: `http://data.europa.eu/esco/skill/1605025e-a179-421f-8f35-5b07d182a6b2`. 

## ESCO-kompetenser i Taxonomy
I Taxonomy har vi lagt in samtliga ESCO-kompetenser som egna begrepp/koncept och representerat mappningarna till Taxonomy-begreppen med hjälp av mappningsrelationer mellan begreppen. URI:erna till ESCO-begreppen hittar man sedan kopplade till attributet `esco-uri` i konceptet. 

Exemplet nedan visar hur ett sådant koncept kan se ut i Taxonomy. Notera att det utöver `esco-uri` finns ett attribut för att representera URI:er (`uri`), detta kan användas av externa källor för att länka till begrepp i Taxonomy. 

```json 
{
    "id": "oFJV_raT_EUx",
    "preferred_label": "webbanalys",
    "esco_uri": "http://data.europa.eu/esco/skill/1605025e-a179-421f-8f35-5b07d182a6b2",
    "uri": "http://data.jobtechdev.se/taxonomy/concept/oFJV_raT_EUx",
    "type": "esco-skill"
}

```

Samma införlivande av ESCO-begreppen i Taxonomy gäller inte för hela den hierarkiska struktur som beskriver ESCO-kompetensernas sammanhang, det vill säga de överordnade nivåerna i strukturen. Det är enbart de detaljerade ESCO-kompetenserna som kan hittas i Taxonomy. Anledningen är dels att det är dessa detaljerade begrepp som skyldigheten att mappa avser, men också att vi haft som princip att enbart införliva den information som krävs, kopplat till ESCO-begreppen, för att kunna utföra mappningarna. Det är själva länkningen som är intressant. Eventuell metadata kring ett visst begrepp hämtas istället från ESCO genom länkningen.

## Mappningar
[Möjliga mappningsrelationer kan hittas här](https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/-/blob/main/doc_sv/relationer.md)

En ESCO-kompetens kan vara mappad mot flera kompetensbegrepp, och vice versa. 

I ESCO finns nästan 14000 kompetenser, medans antalet kompetensbegrepp i Taxonomy är runt 6000. Den högre detaljeringsgraden i ESCO-kompetenserna innebär att en majoritet av mappningarna från dem är med relationen `broad-match`, dvs: kompetensbegreppet är bredare än ESCO-kompetensen. 

I följande exempel har däremot ESCO-kompetensen [webbanalys](http://data.europa.eu/esco/skill/1605025e-a179-421f-8f35-5b07d182a6b2) mappats till kompetensbegreppen [Konverteringsoptimering](http://data.jobtechdev.se/taxonomy/concept/Hb14_vsa_TwV) och [Google Analytics](http://data.jobtechdev.se/taxonomy/concept/oCKN_G9k_zTj) med hjälp av relationen `narrow-match`. `narrow-match` är en invers av relationen `broad-match`, vilket medför att både [Konverteringsoptimering](http://data.jobtechdev.se/taxonomy/concept/Hb14_vsa_TwV) och [Google Analytics](http://data.jobtechdev.se/taxonomy/concept/oCKN_G9k_zTj) är mappade med `broad-match` till [webbanalys](http://data.europa.eu/esco/skill/1605025e-a179-421f-8f35-5b07d182a6b2) sett från deras perspektiv.

```mermaid
graph TD;
    A(webbanalys) -->|narrow-match| B(Google Analytics)
    A -->|narrow-match| C(Konverteringsoptimering)
```
    
### Förvaltning av mappningar

Mappningsarbetet har utförts av Enheten Jobtech på Arbetsförmedlingen, som också bär förvaltningsansvaret. 

I och med att samtliga [kompetensbegrepp](https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/-/blob/main/doc_sv/kompetensbegrepp.md) behöver vara mappade mot en eller flera ESCO-kompetenser så tillkommer hela tiden nya mappningar i samband med att nya kompetensbegrepp skapas eller blir [deprecated](länk). Därför kan det förväntas att varje ny Taxonomy-version innebär ändrade mappningar i någon, oftast mindre, utsträckning.

Mappningarna har utförts huvudsakligen genom manuell analys och enligt [ESCO:s instruktioner](https://esco.ec.europa.eu/system/files/2021-07/425b7a5f-3048-4377-a816-5402c00e9a9505_A_Annex_Draft_ESCO_Implementation_manual.pdf).


### ESCO-kompetenser utan mappningar
För vissa ESCO-kompetenser har bedömningen varit att mappning mot ett eller flera kompetensbegrepp inte varit möjlig. Utöver att dessa begrepp inte har några mappningsrelationer har de ett särskilt attribut, `no-esco-relation` för att enklare kunna särskilja dem från de mappningsbara begreppen. Se exemplet nedan.

```json
      {
        "id": "FcHd_Pat_syt",
        "preferred_label": "utvärdera sin egen miljöpåverkan",
        "esco_uri": "http://data.europa.eu/esco/skill/2aaad52d-599e-4e77-a681-bbc236825821",
        "uri": "http://data.jobtechdev.se/taxonomy/concept/FcHd_Pat_syt",
        "type": "esco-skill",
        "no_esco_relation": true
      }
```

## ESCO-versioner 
I samband med att en ny [ESCO-version](https://esco.ec.europa.eu/en/about-esco/escopedia/escopedia/esco-versions) publiceras uppdateras innevarande Taxonomy-version med eventuellt nya ESCO-kompetenser. Mappningar som speglar den nya ESCO-version kommer på så vis vara tillgängliga i samband med att den innevarande Taxonomy-versionen publiceras.  

## Exempel på GrahphQL-queries
[Exemplen nedan kan testas i vårat GraphiQL-gränssnitt.](https://taxonomy.api.jobtechdev.se/v1/taxonomy/graphiql)
### Alla ESCO-mappningar

```json
query MyQuery {
  concepts(type: "skill") {
    id
    preferred_label
    uri
    type
    broad_match (type: "esco-skill"){
      id
      preferred_label
      esco_uri
      type
    }
    narrow_match (type: "esco-skill"){
      id
      preferred_label
      esco_uri
      type
    }
    exact_match (type: "esco-skill"){
      id
      preferred_label
      esco_uri
      type
    }
    close_match (type: "esco-skill"){
      id
      preferred_label
      esco_uri
      type
    }
  }
}
```
