# ESCO-yrken

## Definition

Ett ESCO-yrke är likställd med en [esco occupation](https://esco.ec.europa.eu/en/classification/occupation_main) (den mest detaljerade nivån), med svensk `preferred-label.
Det är EU-kommissionen som står som ägare av begreppet.

## Beskrivning
ESCO](https://esco.ec.europa.eu/sv) är en EU-gemensam klassificering av yrken och kompetenser, motsvarande Jobtech Taxonomy. Begreppen i ESCO är översatta till samtliga EU:s officiella språk plus bland annat arabiska och utgör ett nav för interoperabilitet mellan en nationell och en (inom EU) internationell nivå. 

EU:s medlemsstater är genom [Eures-direktivet](https://eur-lex.europa.eu/eli/reg/2016/589/oj) bundna till att antingen skapa mappningar mellan ESCO och sina nationella yrkes- och kompetensstrukturer, eller att helt implementera ESCO. Det huvudsakliga syftet är att CV:n och platsannonser som skickas till Eures ska berikas med referenser (URI:er) till de begrepp i ESCO som mappningsrelationerna från yrkesbenämningar eller kompetensbegrepp från den nationella strukturen, som ingår i CV:t eller platsannonsen, pekar på. I Sverige valde vi att mappa [yrkesbenämningar](https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/-/blob/main/doc_sv/yrkesben%C3%A4mningar.md) och [kompetensbegrepp](https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/-/blob/main/doc_sv/kompetensbegrepp.md) till motsvarande begreppstyper i ESCO. Mappningarna mellan begreppsstrukturerna tillgängliggörs via [Jobtech Taxonomy](https://jobtechdev.se/sv/komponenter/jobtech-taxonomy). Medlemsstaters mappningstabeller publiceras på [ESCOs hemsida](https://esco.ec.europa.eu/sv/node/408).

Yrken i ESCO är strukturerade enligt ISCO (International Standard Classification of Occupations), den internationella standarden för klassificering av yrken.1 Yrkesbenämningar i Arbetsförmedlingens klassifikation är strukturerad enligt SSYK (Svensk standard för yrkesklassificering), vilken bygger på [ISCO]( https://eur-lex.europa.eu/legal-content/SV/TXT/?uri=CELEX%3A32016R0589).

Arbetsförmedlingens och ESCOs klassifikationer skiljer sig på flera håll avseende vilken detaljnivå som tillämpas. I många områden är ESCO mer detaljerad, men det förekommer även fall där den är mindre detaljerad än den svenska klassifikationen. 

Begrepp i ESCO identifieras med hjälp av beständiga URI:er, exempelvis: `http://data.europa.eu/esco/occupation/6c999fc7-c6b7-4ef3-a4b9-af124a1783a2`. 


## ESCO-yrken i Taxonomy
I Taxonomy har vi lagt in samtliga ESCO-yrken som egna begrepp/koncept och representerat mappningarna till Taxonomy-begreppen med hjälp av mappningsrelationer mellan begreppen. URI:erna till ESCO-begreppen är kopplade till attributet `esco-uri` i konceptet. 

Exemplet nedan visar hur ett sådant koncept kan se ut i Taxonomy. Notera att det utöver `esco-uri` finns ett attribut för att representera URI:er (`uri`), detta kan användas av externa källor för att länka till begrepp i Taxonomy. 

```json 
{
    "id": "v2kx_tST_Zy2",
    "preferred_label": "kemist",
    "type": "esco-occupation",
    "esco_uri": "http://data.europa.eu/esco/occupation/0d93706d-32fd-4de3-aa08-be1003e325da",
    "uri": "http://data.jobtechdev.se/taxonomy/concept/v2kx_tST_Zy2"
}

```

## Mappningar
[Möjliga mappningsrelationer kan hittas här](https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/-/blob/main/doc_sv/relationer.md#mappningsrelationer)


Ett ESCO-yrke kan vara mappat mot flera [yrkesbenämningar](https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/-/blob/main/doc_sv/yrkesben%C3%A4mningar.md?ref_type=heads), och vice versa. Exemplet nedan visar esco-yrket [kemist](http://data.europa.eu/esco/occupation/0d93706d-32fd-4de3-aa08-be1003e325da) och några av dess mappningar mot [yrkesbenämningar](https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/-/blob/main/doc_sv/yrkesben%C3%A4mningar.md?ref_type=heads).

```mermaid
graph TD;
    A(kemist) -->|narrow-match| B(Materialkemist)
    A(kemist) -->|narrow-match| C(Organisk kemist)
    A -->|exact-match| D(Kemist)
```

### Förvaltning av mappningar

Mappningsarbetet har utförts av Enheten Jobtech på Arbetsförmedlingen, som också bär förvaltningsansvaret. 

I och med att samtliga [yrkesbenämningar](https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/-/blob/main/doc_sv/yrkesbenämningar.md) behöver vara mappade mot en eller flera ESCO-yrken så tillkommer hela tiden nya mappningar i samband med att nya yrkesbenämningar skapas eller blir [deprecated](länk). Därför kan det förväntas att varje ny Taxonomy-version innebär ändrade mappningar i någon, oftast mindre, utsträckning.

Mappningarna har utförts huvudsakligen genom manuell analys och enligt [ESCO:s instruktioner](https://esco.ec.europa.eu/system/files/2021-07/425b7a5f-3048-4377-a816-5402c00e9a9505_A_Annex_Draft_ESCO_Implementation_manual.pdf).

## ESCO-versioner 
I samband med att en ny [ESCO-version](https://esco.ec.europa.eu/en/about-esco/escopedia/escopedia/esco-versions) publiceras uppdateras innevarande Taxonomy-version med eventuellt nya ESCO-yrken. Mappningar som speglar den nya ESCO-version kommer på så vis vara tillgängliga i samband med att den innevarande Taxonomy-versionen publiceras.  


## Exempel på GrahphQL-queries
[Exemplen nedan kan testas i vårat GraphiQL-gränssnitt.](https://taxonomy.api.jobtechdev.se/v1/taxonomy/graphiql)
### Alla ESCO-mappningar

```json
query MyQuery {
  concepts(type: "occupation-name") {
    id
    preferred_label
    uri
    type
    broad_match (type: "esco-occupation"){
      id
      preferred_label
      esco_uri
      type
    }
    narrow_match (type: "esco-occupation"){
      id
      preferred_label
      esco_uri
      type
    }
    exact_match (type: "esco-occupation"){
      id
      preferred_label
      esco_uri
      type
    }
    close_match (type: "esco-occupation"){
      id
      preferred_label
      esco_uri
      type
    }
  }
}
```


