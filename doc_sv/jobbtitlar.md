# Jobbtitlar

## Definition
En titel som refererar till ett visst jobb, oftast hämtad från platsannonser.

## Beskrivning
En jobbtitel kan ses som en variation av en [yrkesbenämning](https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/-/blob/main/doc_sv/yrkesben%C3%A4mningar.md). Jobbtiteln är oftast mer avgränsad, ibland specifik för en viss organisation, och inte lika vedertagen (sett till en bredare del av arbetsmarknaden) som den yrkesbenämning till vilken den är kopplad. En jobbtitel ska inte ses som en direkt synonym till en yrkesbenämning (för synonymisering finns istället [attributet](https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/-/blob/main/doc_sv/attribut.md) alternativa benämningar/`alternative-labels`). 

Exemplet nedan vissar hur en jobbtitel relaterar till en yrkesbenämning.

```mermaid
graph LR;
B --related--> A
    subgraph "Yrkesbenämning" 
    A("Journalist")
    end
    subgraph "Jobbtitel"
    B("Allmänreporter");
    end

```
En jobbtitel är alltid kopplad till en yrkesbenämning med relationstypen `related`.


## Syfte
Jobbtitlarna finns till för att bredda det språk som används för att representera yrke i Taxonomy. Idén är att fånga upp varianter av en yrkesbenämning och koppla ihop jobbtiteln med den. På så vis skapas underlag för att bygga smartare slutanvändartillvända tjänster som i högre grad inkluderar användarens begreppsval, samtidigt som hänvisning till en yrkesbenämning kan göras. 

## Förvaltning
En majoritet av jobbtitlarna är hämtade från [JobAd enrichments synonymordlista](https://jobad-enrichments-api.jobtechdev.se/). Begrepp från listan har bedömts manuellt och kopplats mot en eller flera yrkesbenämningar i Taxonomy.

För att bedöma om huruvida ett visst begrepp lämpar sig bättre som jobbtitel eller som yrkesbenämning tas bland annat i beaktning hur vedertaget begreppet verkar vara, genom att se till olika källor på internet och platsannonser. Begreppets detaljeringsnivå kan också vara till hjälp vid bedömningen, då ett allt för specifikt begrepp lämpar sig bättre som jobbtitel.

### Koppling till synoymordlista
Observera att det inte finns någon referens/länk mellan jobbtiteln i Taxonomy och begreppet i synonymordlistan. Jobbtitlarna som hämtats från synonymordlistan har helt enkelt skapats i Taxonomy som egna, fristående begrepp, och det är bara genom deras `preferred-label` som de kan matchas mot motsvarande begrepp i synonymordlistan. Det är dock inte givet att `preferred-label` är exakt samma som motsvarande benämning i synonymordlistan då det förekommer fall där titeln behövts anpassas till benämningskonventioner i Taxonomy.

## Sökbegrepp och jobbtitlar
Precis som sökbegreppen är [jobbtitlar](https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/-/blob/main/doc_sv/jobbtitlar.md) begrepp som kan användas för att referera till yrkesbenämningar. Begreppstyperna skiljer sig dock på följande punkter:

- sökbegrepp kan vara kopplade till flera begreppstyper, medan jobbtitlar enbart är kopplade till yrkesbenämningar
- jobbtitlar är mycket mer specifika än sökbegrepp och representerar ofta en smalare variant av en yrkesbenämning
- en jobbtitel refererar oftast till enbart en yrkesbenämning, medan sökbegreppen oftast refererar till flera

Nedan följer ett antal exempel som visar på skillnaderna.

Avseende jobbtitlar:


| **Jobbtitel**    | **Yrkesbenämning** |
| ---------------- | ------------------- |
| [Allmänreporter](http://data.jobtechdev.se/taxonomy/concept/yVkt_1Qf_eps)   | [Journalist/Reporter](http://data.jobtechdev.se/taxonomy/concept/BduY_UUY_pvK) |
| [Industribrandman](http://data.jobtechdev.se/taxonomy/concept/Lv4F_cod_oix) | [Brandman](http://data.jobtechdev.se/taxonomy/concept/sgib_x6a_wnW)            |
| [Industrilärare](http://data.jobtechdev.se/taxonomy/concept/wG1s_sGy_HaR)   | [Yrkeslärare](http://data.jobtechdev.se/taxonomy/concept/Hob8_zfc_5ge)         |

Avseende sökbegrepp:

| **Sökbegrepp** | **Yrkesbenämning**     |
| ----------- | ----------------------- |
| [Avlopp](http://data.jobtechdev.se/taxonomy/concept/HQKe_vYM_ojU)      | [Avloppstekniker](http://data.jobtechdev.se/taxonomy/concept/fS28_kGG_xYG)         |
| [Avlopp](http://data.jobtechdev.se/taxonomy/concept/HQKe_vYM_ojU)      | [Sug- och spolbilsförare](http://data.jobtechdev.se/taxonomy/concept/S8DM_Z9a_3W6) |
| [Järnväg](http://data.jobtechdev.se/taxonomy/concept/HSeG_zAZ_SpC)     | [Fjärrtågklarerare](http://data.jobtechdev.se/taxonomy/concept/5VG3_9vV_18L)       |
| [Järnväg](http://data.jobtechdev.se/taxonomy/concept/HSeG_zAZ_SpC)     | [Bangårdsoperatör](http://data.jobtechdev.se/taxonomy/concept/rcCk_3nM_uqj)        |

Observera att ovan visas enbart ett avgränsat antal relationer, exempelvis "Avlopp" och "Järnväg" är relaterade till många fler yrkesbenämningar.

## GraphQL-exempel
Exemplen nedan kan testas i vårat [GraphiQL-gränssnitt](https://taxonomy.api.jobtechdev.se/v1/taxonomy/graphiql).

### Alla yrkesbenämningar med kopplingar till jobbtitlar

```graphql
query MyQuery {
  concepts(type: "occupation-name") {
    id
    preferred_label
    type
    related (type: "job-title"){
      id 
      preferred_label
      type
    }
  }
}


```


