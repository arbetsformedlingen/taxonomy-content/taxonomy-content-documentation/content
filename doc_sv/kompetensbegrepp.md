# Kompetensbegrepp

<*Under uppbyggnad*>


- kopplingar till andra begrepp (ssyk, yrkesbenämning, esco)
- *kompetens* som begrepp
- Kompetensbegrepp utan ESCO-mappningar

## Definition

## Beskrivning

## Begränsningar

- idag mycket bred användning av begreppet "kompetensbegrepp"
- hur ser användningen ut (bland annat vid annonsering)
- brist på struktur och avgränsningar
- brist på definitioner
