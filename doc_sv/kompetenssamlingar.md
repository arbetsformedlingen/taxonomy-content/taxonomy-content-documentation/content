# Kompetenssamlingar

## Definition
En kompetenssamling beskriver en särskild kategorisering av kompetensbegrepp, fristående från den ordinarie kompetensstrukturen. 

## Beskrivning
En kompetenssamling kan skapas vid behov av kategoriseringar av [kompetensbegrepp](https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/-/blob/main/doc_sv/kompetensbegrepp.md), utöver de grupperingar som redan finns i kompetensstrukturen. På så vis möjliggör kompetenssamlingen för mer flexibilitet vad gäller ickehierarkiska skärningar av den massa som kompetensbegreppen utgör. En kompetenssamling skapas som ett eget begrepp i Taxonomy, se exempelvis Kompetenser med grön koppling (länk till atlas behövs här), och kan ses som en sorts metadata för de kompetensbegrepp som är kopplade till samlingen. 


## ESCO-baserade kompetenssamlingar
Team Redaktionen på enheten JobTech har kartlagt ESCOs gröna respektive digitala kompetenser mot kompetensbegrepp i Arbetsförmedlingens taxonomi. Kartläggning har skett enligt [ESCOs metoddokument](https://esco.ec.europa.eu/system/files/2021-07/425b7a5f-3048-4377-a816-5402c00e9a9505_A_Annex_Draft_ESCO_Implementation_manual.pdf ). Arbetet har i huvudsak varit manuellt, med stöd i form av maskinellt framtagna förslag på relevanta kartläggningar. Processen är sådan att utifrån de beskrivningar som finns för ESCOs kompetensbegrepp görs bedömningar avseende lämpliga kopplingar till Arbetsförmedlingens kompetensbegrepp. 

För att kartlägga begrepp i nationella klassifikationer mot begrepp i ESCO används relationer för att beskriva hur väl innebörden av begreppen stämmer överens. De fyra olika relationerna uttrycks så att ett begrepp (1) är mer generellt, (2) är mer specialiserat, (3) motsvarar eller (4) är snarlikt. Exempel på relationer utifrån kartläggning av gröna ESCO-kompetenser:

| Kompetensbegrepp | Relationstyp | ESCO-kompetens |
| ------ | ------ | ------ |
| [Berggrundsgeologi](http://data.jobtechdev.se/taxonomy/concept/uWaM_WHp_tV4) | [mer generell/broad-match](https://www.w3.org/2009/08/skos-reference/skos.html#broadMatch) | [geologi](http://data.europa.eu/esco/skill/8bdd25f8-4b6e-40fe-a2cf-c1badc594114)|
| [Avfallsteknik](http://data.jobtechdev.se/taxonomy/concept/iW5w_mih_Lzk) | [mer specialiserad/narrow-match](http://www.w3.org/2004/02/skos/core#narrowMatch) | [utveckla förfaranden för avfallshantering](http://data.europa.eu/esco/skill/114a79ef-1e62-475b-a862-954f5b4cca20) |
| [Miljökonsekvensanalys](http://data.jobtechdev.se/taxonomy/concept/yMp7_AwC_zqx) | [motsvarar/exact-match](http://www.w3.org/2004/02/skos/core#exactMatch) | [bedöma miljöpåverkan](http://data.jobtechdev.se/taxonomy/concept/rggf_41g_m6p) |
| [Jordbruksturism](http://data.jobtechdev.se/taxonomy/concept/ZZ3K_X56_6BC) | [snarlik/close-match](http://www.w3.org/2004/02/skos/core#closeMatch ) | [tillhandahålla tjänster inom landsbygdsturism](http://data.europa.eu/esco/skill/8550a96d-3970-46e1-83e1-371c59473ec4)|

Kartläggning enligt formatet ovan ska läsas så att för kompetensbegreppet [Berggrundsgeologi](http://data.jobtechdev.se/taxonomy/concept/uWaM_WHp_tV4) finns en relation till den mer generella ESCO-kompetensen [geologi](http://data.europa.eu/esco/skill/8bdd25f8-4b6e-40fe-a2cf-c1badc594114). ESCO-kompetensens innebörd är alltså bredare än innebörden av kompetensbegreppet. Den omvända relationen gäller även mellan begreppen, det vill säga att innebörden av kompetensbegreppet [Berggrundsgeologi](http://data.jobtechdev.se/taxonomy/concept/uWaM_WHp_tV4) är smalare än innebörden av ESCO-kompetensen [geologi](http://data.europa.eu/esco/skill/8bdd25f8-4b6e-40fe-a2cf-c1badc594114).

För [Avfallsteknik](http://data.jobtechdev.se/taxonomy/concept/iW5w_mih_Lzk) finns en relation till den mer specialiserade ESCO-kompetensen [utveckla förfaranden för avfallshantering](http://data.europa.eu/esco/skill/114a79ef-1e62-475b-a862-954f5b4cca20). Innebörden av ESCO-kompetensen är då smalare än innebörden av kompetensbegreppet. 

För kompetensbegreppet [Miljökonsekvensanalys](http://data.jobtechdev.se/taxonomy/concept/yMp7_AwC_zqx) finns en relation till den motsvarande ESCO-kompetensen [bedöma miljöpåverkan](http://data.jobtechdev.se/taxonomy/concept/rggf_41g_m6p). Begreppen uttrycks olika men bedöms ha samma innebörd. 

För kompetensbegreppet [Jordbruksturism](http://data.jobtechdev.se/taxonomy/concept/ZZ3K_X56_6BC) finns en snarlik relation till ESCO-kompetensen [tillhandahålla tjänster inom landsbygdsturism](http://data.europa.eu/esco/skill/8550a96d-3970-46e1-83e1-371c59473ec4). Det bedöms finnas delar av innebörden i respektive begrepp som motsvarar varandra, till exempel vissa arbetsuppgifter, men även delar som inte motsvarar. 

Utifrån kartläggning framgår att det är vanligt att flera ESCO-kompetenser kartläggs mot ett eller flera bredare kompetensbegrepp. Ett exempel på detta är kompetensbegreppet [Avfallshantering](http://data.jobtechdev.se/taxonomy/concept/YYdi_Fqb_834) som har relationer till fler än tjugo olika ESCO-kompetenser.  Detta är ett väntat resultat då det är en relativt stor skillnad i detaljnivå mellan kompetensbegrepp och ESCO-kompetenser. 

### Kompetenssamlingarna 
De ESCO-baserade kompetenssamlingarna är för närvarande:

- Kompetenser med grön koppling
- Kompetenser med digital koppling

Ett kompetensbegrepp som är relaterat till kompetenssamlingen Kompetenser med grön koppling  eller Kompetenser med digital koppling är [mappat](https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/-/blob/main/doc_sv/esco_kompetenser.md#mappningar) mot en, eller flera, [ESCO-kompetenser](https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/-/blob/main/doc_sv/esco_kompetenser.md) som klassificerats som antingen grön/gröna eller digital/digitala.

I exemplet nedan illustreras med hjälp av kompetensbegreppet [Miljökonsekvensanalys](http://data.jobtechdev.se/taxonomy/concept/yMp7_AwC_zqx) hur mappning ([exact-match](http://www.w3.org/2004/02/skos/core#exactMatch )) mot en grön ESCO-kompetens, i det här fallet [bedöma miljöpåverkan](http://data.europa.eu/esco/skill/e541c69c-ea80-4b17-87cb-4001d0b9d303), resulterar i att kompetensbegreppet kopplas till kompetenssamlingen Kompetenser med grön koppling. Exakt samma princip gäller för kompetenssamlingen Kompetenser med digital koppling.

```mermaid
graph LR;
B --related--> A
B --exact-match--> C
C --related--> D
    subgraph "Kompetenssamling" 
    A("Kompetenser med grön koppling")
    end
    subgraph "Kompetensbegrepp"
    B("Miljökonsekvensanalys");
    end
    subgraph "ESCO-kompetens"
    C("bedöma miljöpåverkan");
    end
    subgraph "ESCO-kompetenssamling"
    D("Gröna ESCO-kompetenser");
    end
   
    
```
## Exempel på GrahphQL-queries

[Exemplen nedan kan testas i vårat GraphiQL-gränssnitt.](https://taxonomy.api.jobtechdev.se/v1/taxonomy/graphiql)

### Hämta alla kompetenssamlingar

```graphql
query MyQuery {
  concepts(version: "next", type: "skill-collection") {
    id
    preferred_label
    definition
    type
  }
}

```


### Hämta alla kompetensbegrepp med grön koppling

Vi utgår ifrån `yXbD_NTH_ijE`, som är Id:t för kompetenssamlingen Kompetenser med grön koppling, och frågar efter alla kompetensbegrepp som har `related`-koppling till samlingen.

```graphql
query MyQuery {
  concepts(id: "yXbD_NTH_ijE") {
    id
    preferred_label
    type
    related(type: "skill"){
      id
      preferred_label	
      type
    }
  }
}
```
### Hämta alla kompetensbegrepp med digital koppling

Vi utgår ifrån `4Jty_HpK_a3Q`, som är Id:t för kompetenssamlingen Kompetenser med digital koppling, och frågar efter alla kompetensbegrepp som har `related`-koppling till samlingen.

```graphql
query MyQuery {
  concepts(id: "4Jty_HpK_a3Q") {
    id
    preferred_label
    type
    related(type: "skill"){
      id
      preferred_label	
      type
    }
  }
}
```
