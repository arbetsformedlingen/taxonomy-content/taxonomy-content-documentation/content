# Prognosyrken

<*under uppbyggnad*>

## Definition
Ett prognosyrke (även kallat `forecast-occupation`) beskriver en eller fler ihopslagna yrkesgruppers konkurrensläge på arbetsmarknaden. Ett prognosyrke riktas aldrig mot en lägre nivå i SSYK-strukturen än SSYK 4 men kan däremot bestå av en eller fler [SSYK-4-grupper](https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/-/blob/main/doc_sv/ssyk_niv%C3%A5_4.md).

## Beskrivning

- Hur är de sammansatta i taxonomy
- Prognosyrken kopplas enbart till SSYK-4-grupper

## Förvaltning

Prognosyrkena förvaltas av Analysavdelningen på Arbetsförmedlingen. 

### Uppdateringsförfarande

Vid behov kan nya prognosyrken läggas till

## Exempel på GrahphQL-queries

[Exemplen nedan kan testas i vårat GraphiQL-gränssnitt](https://taxonomy.api.jobtechdev.se/v1/taxonomy/graphiql).

### Hämta alla prognosyrken

```graphql
query MyQuery {
  concepts(type: "forecast-occupation") {
    id
    preferred_label
    type
    related(type: "ssyk-level-4") {
      id
      preferred_label
      type
      ssyk_code_2012
    }
  }
}
```

