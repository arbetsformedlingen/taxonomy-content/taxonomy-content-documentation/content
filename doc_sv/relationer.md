# Relationer

## Definition
Olika typer av kopplingar mellan begrepp i Taxonomy-databasen.

## Beskrivning
Relationer i Taxonomy-databasen uttrycks som egna entiteter mellan olika begrepp. I huvudsak utgår relationstyperna från [SKOS](https://www.w3.org/TR/skos-reference/) och ärver de egenskaper som beskrivs där, men i och med att behov uppstått har även lokala relationstyper definierats. 

## Relationstyper

En lista över relationstyper kan hämtas via [Taxonomy-API:et](https://taxonomy.api.jobtechdev.se/v1/taxonomy/main/relation/types).

Observera att vissa av relationerna inte finns med i [listan från Taxonomy-API:et](https://taxonomy.api.jobtechdev.se/v1/taxonomy/main/relation/types). Anledningen är att relationstyper som är en invers av en annan relationstyp inte uttrycks explicit i Taxonomy-databasen. Exempelvis används `broader` men inte `narrower`. Däremot: om en `broader`-relation finns från begrepp A till begrepp B, så finns relationen `narrower` implicit mellan begrepp B och begrepp A. 

I exemplet nedan har relationen mellan [yrkesbenämningen](https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/-/blob/main/doc_sv/yrkesben%C3%A4mningar.md) [Dansare](http://data.jobtechdev.se/taxonomy/concept/euJP_wxo_skF) och [SSYK-4-gruppen](https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/-/blob/main/doc_sv/ssyk_niv%C3%A5_4.md) [Koreografer och dansare](http://data.jobtechdev.se/taxonomy/concept/gmRr_7tt_eHj) definierats som `broader` i Taxonomy-databasen. 

```mermaid
graph TB;
    subgraph "Explicit relation" 
    A --broader--> B
    A("Dansare (yrkesbenämning)")
    B("Koreografer och dansare (ssyk-nivå-4)");
    end
```

I och med att `narrower` är en invers av `broader` finns den implicit sett från andra hållet.  


```mermaid
graph TB;
    subgraph "Implicit relation" 
    B --narrower--> A
    A("Dansare (yrkesbenämning)")
    B("Koreografer och dansare (ssyk-nivå-4)");
    end 
```

I [Taxonomy-API:et](https://taxonomy.api.jobtechdev.se/v1/taxonomy/graphiql) går det ange såväl `broader` som `narrower` för att hämta relationerna mellan begreppen, till exempel:

```graphql

query MyQuery {
  concepts(id: "euJP_wxo_skF") {
    id
    preferred_label
    type
    broader (id: "gmRr_7tt_eHj") {
      id
      preferred_label
      type
    }
  }
}

```

Och: 

```graphql

query MyQuery {
  concepts(id: "gmRr_7tt_eHj") {
    id
    preferred_label
    type
    narrower (id: "euJP_wxo_skF") {
      id
      preferred_label
      type
    }
  }
}

```


### SKOS-relationer

En majoritet av de relationstyper som finns i Taxonomy är hämtade från [SKOS](https://www.w3.org/TR/skos-reference/). Relationstypernas egenskaper, såsom de är definierade i [SKOS](https://www.w3.org/TR/skos-reference/), gäller även i Taxonomy. Se länkar i tabellen nedan för att läsa mer. 

| taxonomy | skos |
| ------ | ------ |
| broader | [skos:broader](http://www.w3.org/2004/02/skos/core#broader ) |
| narrower | [skos:narrower](http://www.w3.org/2004/02/skos/core#narrower ) |
| related | [skos:related](http://www.w3.org/2004/02/skos/core#related ) |
| exact-match | [skos:exactMatch](http://www.w3.org/2004/02/skos/core#exactMatch ) |
| broad-match | [skos:broadMatch](http://www.w3.org/2004/02/skos/core#broadMatch) |
| close-match | [skos:closeMatch](http://www.w3.org/2004/02/skos/core#closeMatch ) |
| narrow-match | [skos:narrowMatch](http://www.w3.org/2004/02/skos/core#narrowMatch) |


#### Mappningsrelationer

I [SKOS](https://www.w3.org/TR/skos-reference/) definieras ett antal mappningsrelationer, se nedan. I Taxonomy används dessa för att representera från - och till - en egen klassificeringen till - och från - en extern klassificering. 

| taxonomy | skos |
| ------ | ------ |
| exact-match | [skos:exactMatch](http://www.w3.org/2004/02/skos/core#exactMatch ) |
| broad-match | [skos:broadMatch](http://www.w3.org/2004/02/skos/core#broadMatch) |
| close-match | [skos:closeMatch](http://www.w3.org/2004/02/skos/core#closeMatch ) |
| narrow-match | [skos:narrowMatch](http://www.w3.org/2004/02/skos/core#narrowMatch) |


Mappningsrelationerna hittas i huvudsak mellan yrkesbenämningar<->esco-yrken, och mellan kompetensbegrepp<->esco-kompetenser.


### Andra relationer
Utöver de relationer som tagits in från [SKOS](https://www.w3.org/TR/skos-reference/) finns ett antal relationstyper som är specifika för Taxonomy. Dessa listas nedan.

#### substitutability
`substitutability`, även kallat "benämningssläktskap", är en relation som kan uttryckas mellan två [yrkesbenämningar](https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/-/blob/main/doc_sv/yrkesben%C3%A4mningar.md). En `substitutability`-relation kan vara antingen låg eller hög. Nivåerna definieras i API:et som `substitutability-percentage`, där låg motsvaras av `25` och hög motsvaras av `75`. 

Om två yrkesbenämningar har ett högt, eller starkt, benämningssläktskap innebär det att det finns en stor likhet mellan yrkesbenämningarna, och att en stor del av arbetsuppgifterna är samma eller snarlika. 

Om yrkesbenämningarna har lågt, eller svagt, benämningssläktskap innebär det att viss likhhet finns mellan dem, och att en del av arbetsuppgifterna är samma eller snarlika. 

Benämningssläktskapen ska ses som en indikation om att uppenbar likhet finns, men är långt ifrån ett exakt bedömningsinstrument. I och med att släktskapen sätts manuellt föreligger alltid risken att detaljer missas. Släktskapskartan ska inte ses som komplett. Ytterligare en försvårande aspekt är att bedömningen om huruvida arbetsuppgifter mellan två yrkesbenämningar överlappar görs lite på känsla eftersom att ingen egentlig klassificering över arbetsuppgifter och hur de förhåller sig till olika yrkesbenämningar finns. Därför är det troligt att bara de mest uppenbara släktskapen kan hittas med hjälp av den här relationstypen. 

`substitutability` är exempel på en ickesymmetrisk relationstyp. Det innebär att en relation från `yrkesbenämning A` till `yrkesbenämning B` inte implicerar en relation från `yrkesbenämning B` till `yrkesbenämning A`, dvs det omvända. Detta kan verka ologiskt med tanke på att släktskapen definieras utifrån en likhet i arbetsuppgifter mellan två yrkesbenämningar. Anledningen till att det ser ut som det gör är att släktskapen är framtagna ur ett rekryteringshänseende, där resonemanget har varit att den besläktade yrkesbenämningen (besläktad till den yrkesbenämning som en arbetgivare anger i en annons) i *hög* eller *någon* grad skulle kunna vara potentiell för den utlysta tjänsten. I ett digitalt matchningssammanhang blir det då möjligt att bredda den pool av arbetssökande som beaktas för den utlysta tjänsten. Tankesättet beskrivs kanske enklast utifrån ett exempel. 

I fallet [läkemedelskonsulent](http://data.jobtechdev.se/taxonomy/concept/qSbh_gsL_gJS) och [Läkare](http://data.jobtechdev.se/taxonomy/concept/9K3i_XD4_Kyy) finns en koppling från läkaren till läkemedelskonsulenten, men inte tvärtom. Här har bedömningen varit att en del av den kunskap som en läkare besitter går att överföra till det sammanhang en läkemedelskonsulent befinner sig i och att det vid rekrytering av en läkemedelskonsulent skulle vara möjligt att bredda sökningen även till läkare. 

Så även om arbetsuppgifterna och/eller kunskapskraven mellan två yrkesbenämningar kan vara lika så kan en av yrkesbenämningarna, som i det här exemplet, lyda under en reglering med krav på legitimation som gör en överföring i ett rekryteringssammanhang svår (man kan inte anta att en en läkemedelskonsulent har läkarlegitimation).


#### possible-combinations
Relationstypen används enbart för att visa på möjliga kombinationer av utbildningsnivå (`sun-education-level-2`) och utbildningsinriktning (`sun-education-field-3`), huvudsakligen vid registrering av arbetssökandes högsta utbildningsnivå. 

Relationerna bygger på ett underlag som SCB (Statistiska Centralbyrån) tagit fram. Underlaget bygger på uppgifter om vilka utbildningar som finns kodade på respektive utbildningsnivå och utbildningsinriktning. Om en nivå-inriktningskombination saknas ska det tolkas som att ingen utbildning finns kodad med den.

#### unlikely-combinations
Relationstypen används enbart för att visa på sådana kombinationer av utbildningsnivå (`sun-education-level-2`) och utbildningsinriktning (`sun-education-field-3`) som är möjliga men osannolika, huvudsakligen vid registrering av arbetssökandes högsta utbildningsnivå. 

Relationerna bygger på ett underlag som SCB (Statistiska Centralbyrån) tagit fram. Underlaget bygger på uppgifter om vilka utbildningar som finns kodade på respektive utbildningsnivå och utbildningsinriktning. Att en kombination är osannolik innebär att det finns utbildningar kodade med den men ytterst få. Tanken med att representera dessa uppgifter i Taxonomy är för att användare ska kunna bygga gränssnittslösningar som informerar slutanvändaren (exempelvis den arbetssökande) om att vald kombination enbart ska användas i särskilda fall.

## Angående relationer och deprecation
För relationer finns i dagsläget inget stöd för deprecation (som för begrepp). Det innebär att en borttagen relation inte kan spåras i senare versioner än den då borttaget skedde. Det innebär också att relationer för begrepp som blir deprecated inte tas bort, dvs. det ska vara möjligt att i den senaste versionen söka på ett begrepp som i en tidigare version blivit deprecated och samtidigt se de relationer som begreppet haft till andra begrepp. 





