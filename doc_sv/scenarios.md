# Scenarios

## Definition
Identifierade händelser, kopplat till förvaltningen av begrepp, som kan vara av intresse för konsumenter av Taxonomy. 

## Beskrivning
I varje ny [version](https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/-/blob/main/doc_sv/versioner.md) av [Taxonomy](https://jobtechdev.se/sv/komponenter/jobtech-taxonomy) kan förändringar avseende begrepp ske som kan vara bra att känna till. Nedan redogörs för ett antal sådan förändringar. Listan är inte uttömmande utan innefattar de scenarios som konsumenter av [Taxonomy](https://jobtechdev.se/sv/komponenter/jobtech-taxonomy) lyft upp i olika sammanhang.

## Deprecated

Att ett begrepp blir `deprecated` innebär att det flaggas som inaktuellt och att det inte *bör* användas. Ofta har sådana begrepp en relation från sig som heter `replaced-by`, vilken fungerar som en hänvisning till ett begrepp som enligt bedömning bör användas istället. Begrepp som klassats som `deprecated`tas aldrig bort ur databasen och kan vid behov hämtas från Taxonomy-API:et.

### Hänvisning/replaced-by

I Arbetsförmedlingens system har man under lång tid haft en inbyggd logik kring hänvisningar av deprecated begrepp, huvudsakligen i uppgifter om arbetssökande. Om ett begrepp blir deprecated med hänvisning registreras den yrkesbenämning som hänvisningen pekar på som den arbetssökandes nya yrkesbenämning. Om ingen hänvisning finns så kan den arbetssökande eller dennes arbetsförmedlare behöva göra en ny bedömning om lämplig yrkesbenämning.

En hänvisning får bara peka mot *ett* annat begrepp, huvudsakligen med hänsyn till stycket ovan.

## Yrkesbenämningar

### Yrkesbenämning blir deprecated och får hänvisning

Det finns flera situationer som kan leda till att ett begrepp blir deprecated och får en hänvisning till ett nytt eller annat redan befintligt begrepp. Det kan exempelvis röra sig om att ett nytt begrepp med större omfång ersätter det tidigare begreppet.

I exemplet nedan är det nya begreppet, *Dansare*, bredare än *Dansare, show*, som blivit deprecated. 

*Exempel: deprecated med hänvisning:*

```mermaid
graph LR;
    A --replaced-by--> B
    subgraph "Nytt begrepp" 
        B(Dansare);
        end
    subgraph "Deprecated begrepp" 
        A("Dansare, show")
        end
```

Genom [Graph-ql](https://taxonomy.api.jobtechdev.se/v1/taxonomy/graphiql) får du ut begreppen med hjälp av följande query: 

```graphql
query MyQuery {
  concepts(include_deprecated: true, id: "saUi_aP6_zhU") {
    id
    preferred_label
    type
    replaced_by {
      id
      preferred_label
      type
    }
  }
}

```

När namn för två eller flera begrepp bedömts representera ett och samma begrepp kan begreppen slås samman. I exemplet nedan har *Familjepedagog* slagits ihop till ett begrepp tillsammans med *Familjebehandlare*. 

*Exempel: deprecated med hänvisning:*

```mermaid
graph LR;
    A --replaced-by--> B
    subgraph "nytt begrepp" 
        B(Familjebehandlare/Familjepedagog);
        end
    subgraph "deprecated begrepp" 
        A("Familepedagog")
        end
```

Genom [Graph-ql](https://taxonomy.api.jobtechdev.se/v1/taxonomy/graphiql) får du ut begreppen med hjälp av följande query:

```graphql
query MyQuery {
  concepts(include_deprecated: true, id: "TChs_6ci_gJQ") {
    id
    preferred_label
    type
    replaced_by {
      id
      preferred_label
      type
    }
  }
}


```

### Yrkesbenämning blir deprecated utan hänvisning

Det finns fall där yrkesbenämningar flaggas som deprecated utan att bli hänvisade till något annat begrepp. Oftast inträffar det när ett begrepp behöver brytas ned till flera. I och med att hänvisning enbart sker till ett annat begrepp kan det vara problematiskt om det sker mot en av flera potentiella kandidater. I de fallen står det begrepp som blivit deprecated utan hänvisning, dvs. relationen `replaced-by`finns inte mellan begreppen.



*Exempel: deprecated utan hänvisning:*

```mermaid
graph TD;
    subgraph "nya begrepp" 
        B(Kock, storhushåll);
        C(Kock, a la carte)
        end
    subgraph "deprecated begrepp" 
        A("Kock")
        end
```

### Yrkesbenämning byter SSYK-4-grupp

Varje yrkesbenämning är kopplad till en (och enbart en) SSYK-4-grupp. Om en yrkesbenämning bedöms vara placerad i fel SSYK-4-grupp kan den flyttas till en mer lämplig. I exemplet nedan har yrkesbenämningen [Biträdande universitetslektor](http://data.jobtechdev.se/taxonomy/concept/dvXH_mwD_9KU) flyttats från SSYK-4-gruppen [Verkställande direktörer m.fl.](http://data.jobtechdev.se/taxonomy/concept/3i4a_Ufc_qpp) till [Universitet- och högskolelektorer.](http://data.jobtechdev.se/taxonomy/concept/cYCo_PxY_zQd) Troligen lästes yrkesbenämningen felaktigt som rektor istället för lektor. 

Se följande query i [Graph-ql:](https://taxonomy.api.jobtechdev.se/v1/taxonomy/graphiql)

```graphql
query MyQuery {
  concepts(version: "1", include_deprecated: true, id: "dvXH_mwD_9KU") {
    id
    preferred_label
    type
    broader(type: "ssyk-level-4") {
      id
      preferred_label
      ssyk_code_2012
      type
    }
  }
}

```

I version 1 ser svaret ser ut på följande vis:

```json
{
  "data": {
    "concepts": [
      {
        "id": "dvXH_mwD_9KU",
        "preferred_label": "Biträdande universitetslektor",
        "type": "occupation-name",
        "broader": [
          {
            "id": "3i4a_Ufc_qpp",
            "preferred_label": "Verkställande direktörer m.fl.",
            "ssyk_code_2012": "1120",
            "type": "ssyk-level-4"
          }
        ]
      }
    ]
  }
}
```

Om version 2 eller senare anges, blir svaret istället:

```json
{
  "data": {
    "concepts": [
      {
        "id": "dvXH_mwD_9KU",
        "preferred_label": "Biträdande universitetslektor/Biträdande högskolelektor",
        "type": "occupation-name",
        "broader": [
          {
            "id": "cYCo_PxY_zQd",
            "preferred_label": "Universitets- och högskolelektorer",
            "ssyk_code_2012": "2312",
            "type": "ssyk-level-4"
          }
        ]
      }
    ]
  }
}
```

### Yrkesbenämning byter namn

Om en namnändring innebär att begreppets innebörd förändras ska begreppet istället göras till deprecated med hänvisning till ett nytt begrepp med det nya namnet. Vid exempelvis rättstavningar eller förändringar av kosmetisk karaktär kan namnändring (uppdatering av begreppets `preferred-label`) ske.



## TODO

- lägg till länkar till relationsdokument när det är färdigt

- kanske ange ungefärlig frekvens för varje scenario nedan

- förändringsbenägna begrepp (LÄNK)?

- vilka begrepp kan inte förändras (LÄNK)
