# Språksamlingar

## Definition

Språkindelningar framtagna för ett specifikt syfte. 

## Beskrivning

Språksamlingar är framtagna och förvaltas av Arbetsförmedlingen. 

### Tolkspråk

Tolkspråk är en lista över de språk som Arbetsförmedlingen kan beställa tolkning inom, och innehåller en delmängd av alla språk i Taxonomy. 

Tolkspråken är framtagna av Arbetsförmedlingen tillsammans med en tolkleverantör. 

Listan innehåller de språk som tolkleverantörer i Sverige erbjuder tolkning inom. 


## Förvaltning

Listan över tolkspråk kan komma att uppdateras i fall överenskommelse, tillsammans med tolkleverantör, görs om tillägg av tolkspråk. 

## Exempel på GrahphQL-queries
[Exemplen nedan kan testas i vårat GraphiQL-gränssnitt.](https://taxonomy.api.jobtechdev.se/v1/taxonomy/graphiql)

### Hämta språksamlingar

```graphql
query MyQuery {
  concepts(type: "language-collection") {
    id
    preferred_label
    definition
    type
  }
}
```

### Hämta alla tolkspråk med metadata

```graphql
query MyQuery {
  concepts(id: "ngh6_UjL_9bg") {
    id
    preferred_label
    definition
    type
    narrower {
      id
      preferred_label
      definition
      type
      glottolog_uri
      iso_uri
      wikidata_uri
      iso_639_1_2002
      iso_639_2_1998
      iso_639_3_2007
    }
  }
}
```
