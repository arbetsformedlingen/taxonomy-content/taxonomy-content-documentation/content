# SSYK

## Definition
SSYK, eller Standard för svensk yrkesklassificering, är en hierarkisk struktur som används för att beskriva arbetsmarknaden. SSYK ägs av Statistiska Centralbyrån (SCB) men Arbetsförmedlignen använder sig av en variant med antal modifieringar på den fjärde nivån. Mer information om varianten finns i avsnittet om Af-SSYK nedan.  

## Beskrivning
SSYK ska är en förteckning och en indelning av yrken. SSYK ska synliggöra vilka yrken som finns på arbetsmarknaden. Samtidigt ska SSYK fungera som struktur för internationell rapportering av data kopplad till svenska arbetsmarknad. 

Nuvarande version av SSYK, SSYK-2012, är framtagen av bland annat SCB och Arbetsförmedlingen, med utgångspunkt i den internationella förlagan ISCO (International Standard Classification of Occupations). 

I SSYK-strukturen har yrken indelats och grupperats på fyra nivåer. Den mest övergripande nivån består av tio breda yrkesområden. Varje yrkesområde utgör en hieraki som blir mer yrkesspecifik ju längre ned i den man kommer. Diagrammen nedan redogör dels för hur SSYK-strukturen generellt är uppbyggd, och dels för hur en utvald del av den kan se ut.  


```mermaid
graph TD;
    A(Yrkesområde/ensiffrig nivå) -->|narrower| B(Huvudgrupp/tvåsiffrig nivå)
    B -->|narrower| C(Yrkesgrupp/tresiffrig nivå)
    C -->|narrower| D(Undergrupp/fyrsiffrig nivå)
```
*Diagrammet visar hur SSYK-strukturen är uppbygd.* 

---------------------------

```mermaid
graph TD;
    A(2 - Yrken med krav på fördjupad högskolekompetens) -->|narrower| B(26 - Yrken med krav på fördjupad högskolekompetens inom juridik, kultur och socialt arbete m.m.)
    B -->|narrower| C(261 - Jurister)
    C -->|narrower| D(2611 - Advokater)
```
*Diagrammet visar hierarkin för SSYK-4-gruppen [2611 - Advokater](https://atlas.jobtechdev.se/page/taxonomy.html#concept=q8wL_kdi_WaW)* 

---------------------------


## Af-SSYK
Arbetsförmedlingen använder en modifierad variant av den SSYK-strukturens fjärde nivå, [information om den finns här](https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/-/blob/main/doc_sv/af_ssyk.md).  


## SSYK-nivå 4
[Information om hur Arbetsförmedlingen arbetar med SSYK-nivå 4 finns här](https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/-/blob/main/doc_sv/ssyk_niv%C3%A5_4.md). 

## Angående namn i SSYK-strukturen
Ofta, och inte minst inom Arbetsförmedlingen, används begreppet "Yrkesgrupp" för att referera till det som i SSYK-strukturen benämns som "Undergrupp". I Taxonomy kallas dessa grupperingar för *SSYK-nivå- 4* eller `ssyk-level-4` (i databasen och API:et). I samma anda används begreppet "Yrkesområde" inom Arbetsförmedlingen för att referera till samlingar av SSYK-nivå 4-grupper. 


[Information om Arbetsförmedlingens yrkesområden finns här.](https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/-/blob/main/doc_sv/yrkesområden.md)

## Länkar
- [SCB:s dokumentation av SSYK](https://www.scb.se/dokumentation/klassifikationer-och-standarder/standard-for-svensk-yrkesklassificering-ssyk/)


