# SSYK-nivå 4
<*under uppbyggnad*>

## Definition

[SSYK-strukturens](https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/-/blob/main/doc_sv/ssyk.md) fjärde och mest detaljerade nivå. 

## Beskrivning

SSYK nivå 4-grupper, `ssyk-level-4` eller formellt sett "undergrupper" (se [SCB:s dokumentation](https://www.scb.se/dokumentation/klassifikationer-och-standarder/standard-for-svensk-yrkesklassificering-ssyk/)), fyller en viktig roll i många sammanhang, inte minst inom Arbetsförmedlingens hantering av yrke kopplat till prognostisering, statistikföring och analys. I Taxonomy används SSYK nivå 4-grupperna som en sorts nav till vilka många andra begrepp knyts, däribland [yrkesbenämningar](https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/-/blob/main/doc_sv/yrkesben%C3%A4mningar.md?ref_type=heads) och [kompetensbegrepp](https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/-/blob/main/doc_sv/kompetensbegrepp.md?ref_type=heads). 

SCB står som ägare av [SSYK-strukturen](https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/-/blob/main/doc_sv/ssyk.md?ref_type=heads) där SSYK nivå 4 ingår, och tillsammans med dem beslutade Arbetsförmedlingen att använda en aningen modifierad variant av strukturens fjärde nivå i samband med att den strukturen togs fram. Den varianten kallas [Af-SSYK](https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/-/blob/main/doc_sv/af_ssyk.md?ref_type=heads) och är i det stora hela identisk med ordinarie SSYK nivå 4, men skiljer sig i att ett antal grupperingar (i huvudsak gällande chefsyrken) slagits samman, och två grupperingar som inte används. 

Observera att SSYK nivå 4-grupper inom Arbetsförmedlingen också kallas för "yrkesgrupper", "occupation-groups" och i viss mån "yrkesmallar".

Det sistnämnda avser egentligen ett paket av begrepp där SSYK nivå 4-gruppen agerar knytpunkt som sammanför [yrkesbenämningar](https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/-/blob/main/doc_sv/yrkesben%C3%A4mningar.md?ref_type=heads) och [kompetensbegrepp](https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/-/blob/main/doc_sv/kompetensbegrepp.md?ref_type=heads). Inom Arbetsförmedlingens kärnverksamhet är yrkesmallarna väl etablerade och används framförallt som uppslag vid handläggning av arbetssökande, men utanför myndigheten används inte begreppet i någon större utsträckning. 

Alla SSYK nivå 4-grupper (det vill säga begrepp av typen `ssyk-level-4`) i Taxonomy har [attributet](https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/-/blob/main/doc_sv/attribut.md?ref_type=heads) `ssyk-code-2012` som anger vilken fyrsiffrig SSYK-kod gruppen har. Därutöver har de även en definition, `definition`, som skapades i samband med att SSYK-2012 togs fram. De attribut som ingår i SSYK nivå 4-grupperna ändras i regel inte. Förändringar förekommer enbart vid exempelvis stavfel. Men ändring av definition, namn, eller andra sorters förändringar som kan resultera i att omfång eller identifikation av en SSYK nivå 4-grupp förändras, förekommer alltså inte. Se exempel nedan, där SSYK nivå-4 gruppen [Mjukvaru- och systemutvecklare m.fl](http://data.jobtechdev.se/taxonomy/concept/DJh5_yyF_hEM) beskrivs.


```json
{
  "data": {
    "concepts": [
      {
        "id": "DJh5_yyF_hEM",
        "preferred_label": "Mjukvaru- och systemutvecklare m.fl.",
        "definition": "Analyserar, designar och utvecklar IT-system eller delar av system. Utformar och utvecklar lösningar för programvara, applikationer, system och databaser.",
        "type": "ssyk-level-4",
        "ssyk_code_2012": "2512"
      }
    ]
  }
}

```


## Kopplingar till andra typer av begrepp

I och med att SSYK 4-grupperna är centrala begrepp i Taxonomy har de flertalet kopplingar till andra typer av begrepp. 

Nedan följer en illustration över hur ett antal [begreppstyper](https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/-/blob/main/doc_sv/begreppstyper.md?ref_type=heads) förhåller sig till SSYK nivå 4-grupper.


```mermaid
graph TD;
A --narrower--> B
A --related--> C
D --narrower--> A
E --narrower--> A
A --related--> F
    A("SSYK nivå 4");
    B("Yrkesbenämning");
    C("Kompetensbegrepp");
    D("Yrkesområde")
    E("SSYK nivå 3")
    F("Prognosyrke")
```

### Yrkesbenämningar

En [yrkebenämning](https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/-/blob/main/doc_sv/yrkesben%C3%A4mningar.md?ref_type=heads) måste alltid vara kopplad till en (och endast en) SSYK nivå 4-grupp. I och med att yrkesbenämningarna är mer detaljerade än SSYK nivå 4-grupperna är det generellt sett så att det från en SSYK nivå 4-grupp finns kopplingar till flera yrkesbenämningar. Dessa kopplingar är av typen [`narrower`](https://www.w3.org/2009/08/skos-reference/skos.html#narrower). 

### Kompetensbegrepp

Samma princip gäller _generellt sett_ för kompetensbegrepp, men här finns ett antal undantag. De flesta kompetensbegrepp är kopplade till en eller flera SSYK nivå 4-grupper men i de fall kompetensbegreppen är så pass breda att de skulle kunna vara applicerbara inom nästan alla grupper kopplas de inte till någon. Läs mer om kompetensbegrepp och generella kompetenser i [dokumentationen kring kompetensbegrepp](https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/-/blob/main/doc_sv/kompetensbegrepp.md?ref_type=heads). Kompetensbegrepp kopplas till SSYK nivå 4-grupperna med relationstypen [`related`](https://www.w3.org/2009/08/skos-reference/skos.html#related). Anledningen till att [`related`](https://www.w3.org/2009/08/skos-reference/skos.html#related) används och inte [`narrower`](https://www.w3.org/2009/08/skos-reference/skos.html#narrower), som för [yrkesbenämningar](https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/-/blob/main/doc_sv/yrkesben%C3%A4mningar.md?ref_type=heads), är att ett [kompetensbegrepps](https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/-/blob/main/doc_sv/kompetensbegrepp.md?ref_type=heads) relation till en SSYK nivå 4-grupp inte ansetts lika tydligt definierad som en [yrkesbenämnings](https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/-/blob/main/doc_sv/yrkesben%C3%A4mningar.md?ref_type=heads).

### Yrkesområden

Utöver att vara den mest detaljerade nivån i [SSYK-strukturen](https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/-/blob/main/doc_sv/ssyk.md?ref_type=heads) ingår SSYK nivå 4 i en separat hierarki, som tagits fram av Arbetsförmedlingen, där [yrkesområde](https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/-/blob/main/doc_sv/yrkesomr%C3%A5den.md?ref_type=heads) utgör den högsta nivån och [yrkesbenämning](https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/-/blob/main/doc_sv/yrkesben%C3%A4mningar.md?ref_type=heads) den lägsta, se exempel nedan. 


```mermaid
graph TD;
A --narrower--> B
B --narrower--> C
    subgraph "Yrkesområde"
    A("Data/IT");
    end
    subgraph "SSYK 4"
    B("2512 - Mjukvaru- och systemuvecklare m.fl.");
    end
    subgraph "Yrkesbenämning"
    C("Backend-utvecklare");
    end
```

### Prognosyrken

Ytterligare ett exempel på begrepp som kopplas till SSYK nivå 4-grupperna är [Prognosyrken](https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/-/blob/main/doc_sv/prognosyrken.md?ref_type=heads), som kan bestå av en eller flera sådana kopplingar.



## Förvaltning

I och med att de är statiska och ägs av SCB kräver SSYK nivå 4-grupperna i sig ingen förvaltning från Arbetsförmedlingens sida. Däremot förvaltas kontinuerligt _kopplingarna_ till grupperingarna. Exempelvis när en ny [yrkesbenämning](https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/-/blob/main/doc_sv/yrkesben%C3%A4mningar.md?ref_type=heads) eller ett nytt [kompetensbegrepp](https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/-/blob/main/doc_sv/kompetensbegrepp.md?ref_type=heads) skapas, eller när någon av dem [byter SSYK nivå 4-grupp.](https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/-/blob/main/doc_sv/scenarios.md?ref_type=heads). 


## Exempel på GrahphQL-queries

[Exemplen nedan kan testas i vårat GraphiQL-gränssnitt](https://taxonomy.api.jobtechdev.se/v1/taxonomy/graphiql).

### Hämta alla SSYK-4-grupper

```graphql
query MyQuery {
  concepts(type: "ssyk-level-4") {
    id
    preferred_label
    type
    definition
    ssyk_code_2012
  }
}
```

### Hämta alla SSYK-4-grupper med yrkesbenämningar

```graphql
query MyQuery {
  concepts(type: "ssyk-level-4") {
    id
    preferred_label
    type
    definition
    ssyk_code_2012
    narrower(type: "occupation-name") {
      id
      preferred_label
      type
    }
  }
```

### Hämta alla SSYK-4-grupper med kompetensbegrepp

```graphql
query MyQuery {
  concepts(type: "ssyk-level-4") {
    id
    preferred_label
    type
    definition
    ssyk_code_2012
    related(type: "occupation-name") {
      id
      preferred_label
      type
    }
  }
}
```
