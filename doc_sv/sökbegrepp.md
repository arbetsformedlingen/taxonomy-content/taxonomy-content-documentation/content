# Sökbegrepp

## Definition
Ett brett begrepp, ofta beskrivande för ett större, inte nödvändigtvis yrkesspecifikt, fenomen eller område. 

## Beskrivning
Sökbegrepp, eller `keyword` är begrepp som används för att referera till andra begrepp. Ett sökbegrepp kan beskriva exempelvis ett område eller ett fenomen som kan ha koppling till exempelvis en eller flera [yrkesbenämningar](https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/-/blob/main/doc_sv/yrkesben%C3%A4mningar.md). Sökbegreppen är ofta breda, med kopplingar till många andra begrepp. Ett exempel är begreppet [Juridik](https://atlas.jobtechdev.se/page/taxonomy.html#concept=Z9ZH_7XU_Kkb), som har kopplingar till ett stort antal begrepp inom området juridik. Sökbegrepp kan också vara kopplade till [anställningsformer](https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/-/blob/main/doc_sv/arbetsbeskrivningar.md) och [utbildningsinriktningar (inom SUN2020)](https://www.scb.se/dokumentation/klassifikationer-och-standarder/svensk-utbildningsnomenklatur-sun/).

Exemplet nedan visar hur ett sökbegrepp relaterar till olika yrkesbenämningar.

```mermaid
graph LR;
B --related--> A
B --related--> C
B --related--> D
    subgraph "Yrkesbenämningar" 
    A("Ambulanssjuksköterska")
    C(Ambulanssjukvårdare)
    D("Sjuktransportör")
    end
    subgraph "Sökbegrepp"
    B("Ambulans");
    end
```
   
Sökbegrepp är alltid kopplade till yrkesbenämningar med relationstypen `related`.

## Syfte
Sökbegreppen finns till för att bredda det språk som används för att representera olika typer av begrepp i Taxonomy. Ett sökbegrepp kan användas som en bred referens till de olika begrepp till vilket det är kopplat.

## Sökbegrepp och jobbtitlar
Precis som sökbegreppen är [jobbtitlar](https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/-/blob/main/doc_sv/jobbtitlar.md) begrepp som kan användas för att referera till yrkesbenämningar. Begreppstyperna skiljer sig dock på följande punkter:

- sökbegrepp kan vara kopplade till flera begreppstyper, medan jobbtitlar enbart är kopplade till yrkesbenämningar
- jobbtitlar är mycket mer specifika än sökbegrepp och representerar ofta en smalare variant av en yrkesbenämning
- en jobbtitel refererar oftast till enbart en yrkesbenämning, medan sökbegreppen oftast refererar till flera

Nedan följer ett antal exempel som visar på skillnaderna.

Avseende jobbtitlar:

| **Jobbtitel**    | **Yrkesbenämning** |
| ---------------- | ------------------- |
| [Allmänreporter](http://data.jobtechdev.se/taxonomy/concept/yVkt_1Qf_eps)   | [Journalist/Reporter](http://data.jobtechdev.se/taxonomy/concept/BduY_UUY_pvK) |
| [Industribrandman](http://data.jobtechdev.se/taxonomy/concept/Lv4F_cod_oix) | [Brandman](http://data.jobtechdev.se/taxonomy/concept/sgib_x6a_wnW)            |
| [Industrilärare](http://data.jobtechdev.se/taxonomy/concept/wG1s_sGy_HaR)   | [Yrkeslärare](http://data.jobtechdev.se/taxonomy/concept/Hob8_zfc_5ge)         |

Avseende sökbegrepp:

| **Sökbegrepp** | **Yrkesbenämning**     |
| ----------- | ----------------------- |
| [Avlopp](http://data.jobtechdev.se/taxonomy/concept/HQKe_vYM_ojU)      | [Avloppstekniker](http://data.jobtechdev.se/taxonomy/concept/fS28_kGG_xYG)         |
| [Avlopp](http://data.jobtechdev.se/taxonomy/concept/HQKe_vYM_ojU)      | [Sug- och spolbilsförare](http://data.jobtechdev.se/taxonomy/concept/S8DM_Z9a_3W6) |
| [Järnväg](http://data.jobtechdev.se/taxonomy/concept/HSeG_zAZ_SpC)     | [Fjärrtågklarerare](http://data.jobtechdev.se/taxonomy/concept/5VG3_9vV_18L)       |
| [Järnväg](http://data.jobtechdev.se/taxonomy/concept/HSeG_zAZ_SpC)     | [Bangårdsoperatör](http://data.jobtechdev.se/taxonomy/concept/rcCk_3nM_uqj)        |

Observera att ovan visas enbart ett avgränsat antal relationer, exempelvis "Avlopp" och "Järnväg" är relaterade till många fler yrkesbenämningar.
