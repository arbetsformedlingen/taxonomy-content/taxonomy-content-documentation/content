# Versioner

**TODO**

## Definition

## Beskrivning

## Syfte

## Uppdateringar

En ny version av Taxonomy kan innehålla flera olika typer av uppdateringar. Olika [begreppstyper](https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/-/blob/main/doc_sv/begreppstyper.md) uppdateras olika mycket, vissa inte alls. 

Olika typer av uppdateringar, exempelvis:

- attribut uppdateras - vilken påverkan? här finns flera olika typer av scenarios
- relationer skapas och uppdateras 
- begrepp skapas 
- begrepp blir deprecated (attribut)

### Uppdatering av begrepp
Generellt gäller att begrepp som tillhör en extern standard eller struktur som inte förvaltas av Arbetsförmedlingen, exempelvis SUN, [SSYK](https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/-/blob/main/doc_sv/ssyk.md) enbart uppdateras om ett uppenbart fel (exempelvis stavningsfel) föreligger, eller om en uppdatering initieras av den part som äger strukturen, vilket sker i samband med att en ny version av en struktur tas fram, exempelvis SSYK98 -> SSYK2012 och SUN2002 -> SUN2020.

Begrepp som förvaltas av Arbetsförmedlingen uppdateras oftare. Exempel på sådana begrepp är [yrkesbenämningar](https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/-/blob/main/doc_sv/ssyk.md), [kompetensbegrepp](https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/-/blob/main/doc_sv/ssyk.md), [jobbtitlar](https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/-/blob/main/doc_sv/jobbtitlar) och [sökbegrepp.](https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/-/blob/main/doc_sv/s%C3%B6kbegrepp.md) 

### Uppdatering av relationer

Hierarkiska relationer (vilka är dessa?) som redan har skapats uppdateras inte särskilt ofta. En situation där en sådan uppdatering kan ske är om en yrkesbenämning byter [SSYK-4-grupp](https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/-/blob/main/doc_sv/ssyk_niv%C3%A5_4.md) (se [scenarios](https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/-/blob/main/doc_sv/scenarios.md)). Annars 

- länk till begreppstyper som förvaltas av oss 

- bild uppdateringar av *begreppet* och av relationer. 

Varje ny version av Taxonomy uttrycks med ett heltal (exempelvis 20) som kan specificeras vid anrop till [API:et](https://taxonomy.api.jobtechdev.se/index.html). Om version utelämnas svara API:et med den senast publicerade versionen. 



- versionernas omfattning kan variera
- versionnummer uttrycks alltid i heltal
- frekvens ca en gång i månaden
- inlägg på forumet i samband med ny version - informationen hålls på generell nivå
- [versionshistorik](https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/taxonomy-version-history) - mer detaljerad information om varje ny version

[Lista över samtliga versioner finns att tillgå via Taxonomy-API:et.](https://taxonomy.api.jobtechdev.se/v1/taxonomy/main/versions)  
