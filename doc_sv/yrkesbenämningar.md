# Yrkesbenämningar
<*under uppbyggnad*>

## Definition 
<*En definition av "yrkesbenämning" behövs här*.>

## Beskrivning

Yrkesbenämningarna utgör ytterligare en nivå av detaljering under den fjärde nivån av SSYK-strukturen.

En yrkesbenämning är kopplad till:

- en (och enbart en) SSYK-grupp på den fjärde nivån
- en (och enbat en) ISCO-grupp på den fjärde nivån

ISCO-kopplingen sker huvdsakligen för att säkerställa överföring av annonser till EURES-portalen.

Yrkesbenämningarna ska spegla en terminologi som känns igen och används av branscherna. Den grupp som förvaltar begreppen försöker därför stämma av med en organisation som kan representera gällande bransch innan en uppdatering görs, om någon sådan organisation kan hittas. 

### alternative-labels, job-title och keyword

Yrkesbenämningar kan ha [attributet](https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/-/blob/main/doc_sv/attribut.md) `alternative-labels` (alternativa benämningar). De kan även ha kopplingar till begrepp av typen `job-title` (jobbtitel) och `keyword` (sökbegrepp).

[Attributet](https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/-/blob/main/doc_sv/attribut.md) `alternative-labels` används enbart i de fall en uppenbar synonym eller förkortning till begreppet finns. Se exempelvis yrkesbenämningen [Museichef/Museidirektör](http://data.jobtechdev.se/taxonomy/concept/s8Ap_UHv_4Zy). I många fall har dessa yrkesbenämningar de olika termerna i `alternative-labels` utskrivna med "/" i `preferred-label`, men inte alltid.

Till skillnad från `alternative-labels` representerar en `job-title` en benämning som har en annan omfattning än yrkesbenämningen. En `job-title` som har en koppling till en yrkesbenämning ska därför inte ses som en synonym. Se exempelvis begreppet [Länsmuseichef](http://data.jobtechdev.se/taxonomy/concept/APiN_cHa_UB1).

Begrepp som klassificeras som `keyword` utgör ett brett begrepp eller en domän som många yrkesbenämningar kan kopplas till. Exempelvis [Juridik](https://atlas.jobtechdev.se/page/taxonomy.html#concept=Z9ZH_7XU_Kkb).

### Förvaltas av

Team Redaktionen på [JobTechDev](https://jobtechdev.se/sv), Arbetsförmedlingen.

## Syfte

Arbetsförmedlingens yrkesbenämningar har en lång historik. Så länge någon form av digital matchning bedrivits har yrkesbenämningarna, eller tidigare motsvarigheter, varit nödvändiga. Det huvudsakliga syftet bakom dem är att kunna tillhandahålla en terminologi som ska försöka spegla arbetsmarknaden, med tätare uppdateringsintervall än SSYK-strukturen. Den kan användas bland vid jobbmatchning, CV-skapande, kategorisering av jobbanonser och karriärvägledning. 

Yrkesbenämningarna försöker, efter bästa förmåga, spegla vad som är aktuellt på arbetsmarknaden just nu. Det innebär att yrkesbenämningar som bedöms som inaktuella "avaktualiseras", eller ges attributet "deprecated" för att markera att de inte längre bör användas. Begreppen tas däremot aldrig bort. 

## Uppdateringsförfarande

Behovet av uppdatering är kontinuerligt men inte nödvändigtvis jämnt över hela taxonomin. Uppdateringarna sker inte i någon viss ordning utan snarare spontant där behovet uppstår. 

Behov av uppdatering kan signaleras på många sätt, bland annat genom:

- förslag från användare (exempelvis arbetsgivare, arbetssökande eller som konsumerar Taxonomy)
- egen research
- ändringar i styrande dokument (exempelvis för lärare och undersköterskor)
- större genomgångar av begreppsmängden (exempelvis vid mappning mot [ESCO](https://esco.ec.europa.eu/sv))

### Samverkan

Framförallt är det genom samverkan med representanter från branscherna som god kvalitet i yrkesbenämningarna uppnås. I vissa fall finns en sådan samverkan etablerad och i andra fall sker den mer ad-hoc och utifrån bästa förmåga. I de fall det inte finns någon motpart för diskussion inom branschen görs bedömning om huruvida en yrkesbenämning ska inplaceras av Redaktionen. Som underlag till bedömningen används bland annat platsannonser (hur används begreppet?) och andra beskrivande texter som kan hittas avseende begreppet/begreppen som ska placeras in. 

### Kvalitetsnivå

Kvalitet definieras här i förhållande till hur förankrat ett begrepp är med en organisation som representerar den bransch inom vilken yrkesbenämningen är aktuell. Kvalitetsnivån för en yrkesbenämning kan uttryckas med hjälp av tre olika nivåer: 

**Nivå 1**. Enligt nivå 3 och/eller 2, samt: uppdateringen görs i samråd med en  nationell aktör, exempelvis arbetsgivar- och branschorganisation,  yrkesnämnd, utbildningsnämnd eller statlig myndighet, se dokumentet "Kontaktpersoner, remissinstanser".  Nya yrkesbenämningar och flytt av befintliga yrkesbenämningar i  SSYK-strukturen görs i samråd med SCB (yrkesregistret).

**Nivå 2**. Enligt nivå 3 och: uppdateringen görs i samråd med en arbetsgivare  alternativt en rekryterare. Nya yrkesbenämningar och flytt av befintliga yrkesbenämningar i SSYK-strukturen görs i samråd med SCB (yrkesregistret).

**Nivå 3**. Uppdateringen görs som resultat av egen research, d.v.s. kontroll av till exempel annonser på lediga jobb, webbsidor knutna till den  aktuella yrkesbenämningen m.m. Nya yrkesbenämningar och flytt av befintliga yrkesbenämningar i SSYK-strukturen görs i  samråd med SCB (yrkesregistret).

Kvalitetsnivå kallas i API:et för *quality-level*.  *OBS* *att detta i nuläget inte finns för samtliga yrkesbenämningar.*

### Uppdateringsfrekvens

Yrkesbenämningarna uppdateras kontinuerligt i och med att nya versioner av Taxonomy publiceras, ca en gång i månaden. 

De uppdateringar som kommer med en ny version omfattar inte hela begreppsmängden utan de handlar istället om enskilda begrepp och tillägg, namnändringar, avaktualiseringar (deprecation) eller ändringar avseende kopplingar mot andra begrepp. 

I [versionshistoriken](https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/taxonomy-version-history) finns detaljerad information on uppdateringarna per version.

## Standarder

För att beskriva hur ett begrepp relaterar till andra begrepp inom och utanför den egna taxonomin används [SKOS](https://www.w3.org/TR/skos-reference/):

| skos             | taxonomy     |
| ---------------- | ------------ |
| skos:narrower    | narrower     |
| skos:broader     | broader      |
| skos:related     | related      |
| skos:exactMatch  | exact-match  |
| skos:narrowMatch | narrow-match |
| skos:broadMatch  | broad-match  |
| skos:closeMatch  | close-match  |

Mappningsrelationerna, som innehåller "Match", används för att representera en relation till ett begrepp i en annan taxonomi eller terminologi. 

Taxonomy utgår även ifrån SKOS för att definiera concept:

| skos             | taxonomy           |
| ---------------- | ------------------ |
| skos:prefLabel   | preferred-label    |
| skos:altLabel    | alternative-labels |
| skos:hiddenLabel | hidden-labels      |

## Kopplingar mot andra begreppstyper i Taxonomy

- job-titel 
- keyword
- esco-occupation
- ssyk-level-4
- isco-level-4
- occupation-name

## Begränsningar

- Yrkesbenämningarna har inga beskrivningar, attributet "definition" populeras därför av samma term som preferred-label
- Alla begrepp är inte nödvändigtvis helt aktuella, och i vissa fall fattas även begrepp 

## Exempel på graphql-queries

Exemplen nedan kan testas via [vårt graphiql-gränssnitt.](https://taxonomy.api.jobtechdev.se/v1/taxonomy/graphiql)

### SSYK-grupper med tillhörande yrkesbenämningar

```graphql
query MyQuery {
  concepts(type: "ssyk-level-4") {
    id
    type
    preferred_label
    ssyk_code_2012
    narrower(type: "occupation-name") {
      id
      type
      preferred_label
      alternative_labels
    }
  }
}
```

### Yrkesbenämningar med relaterade jobbtitlar och sökbegrepp

```graphql
query MyQuery {
  concepts(type: "occupation-name") {
    id
    type
    preferred_label
    alternative_labels
    related(type: ["keyword", "job-title"]) {
      id
      type
      preferred_label
    }
  }
}
```

### Yrkesbenämningar med ESCO-mappningar

```graphql
query MyQuery {
  concepts(type: "occupation-name") {
    id
    type
    preferred_label
    alternative_labels
    exact_match(type: "esco-occupation") {
      id
      type
      preferred_label
    }
    broad_match(type: "esco-occupation") {
      id
      type
      preferred_label
    }
    narrow_match(type: "esco-occupation") {
      id
      type
      preferred_label
    }
    close_match(type: "esco-occupation") {
      id
      type
      preferred_label
    }
  }
}
```

### Yrkesstruktur med Arbetsförmedlingens yrkesområden

```graphql
query MyQuery {
  concepts(type: "occupation-field") {
    id
    type
    preferred_label
    narrower(type: "ssyk-level-4") {
      id
      type
      preferred_label
      ssyk_code_2012
      narrower(type: "occupation-name") {
        id
        type
        preferred_label
        alternative_labels
      }
    }
  }
}
```


## Länkar

- Jobtech Atlas: https://atlas.jobtechdev.se/
- API: https://taxonomy.api.jobtechdev.se/
- Gitlab: https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend
- Gitlab (förslagstavla): https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-feedback-channel
- Karta över kopplingar mellan begreppstyper: https://atlas.jobtechdev.se/page/about.html
- Forum:  https://forum.jobtechdev.se/
- Kopplingsschema Af-SSYK och SSYK https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/mappings

## Översättningar

| **benämning**           | **api-benämning**  |
| ----------------------- | ------------------ |
| yrkesbenämning          | occupation-name    |
| jobbtitel               | job-title          |
| sökbegrepp              | keyword            |
| alternativa benämningar | alternative-labels |
| SSYK-grupp              | ssyk-level-4       |

