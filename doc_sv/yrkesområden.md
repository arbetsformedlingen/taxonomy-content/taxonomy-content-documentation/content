# Yrkesområden

## Definition
Breda grupperingar av [SSYK-4-grupper](https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/-/blob/main/doc_sv/ssyk_niv%C3%A5_4.md), snarlik en sorts branschindelning på hög nivå.

## Beskrivning
Ett yrkesområde (även kallat `occupation-field`), exempelvis [Data/IT](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF), innehåller ett antal tematiskt närbesläktade SSYK-4-grupper. Bland SSYK-4-grupperna inom [Data/IT](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF) hittas bland annat [2511 - Systemanalytiker och IT-arkitekter m.fl.](http://data.jobtechdev.se/taxonomy/concept/UXKZ_3zZ_ipB) och [2512 - Mjukvaru- och systemutvecklare m.fl.](http://data.jobtechdev.se/taxonomy/concept/DJh5_yyF_hEM)

Yrkesområdena används i huvudsak för att hjälpa användare att navigera i yrkesstrukturen för att hitta rätt yrkesbenämning vid exempelvis annonsering eller skapande av CV. 

Varje SSYK-4-grupp är kopplad till ett (och enbart ett) yrkesområde. 

Exemplet nedan beskriver förhållandet mellan yrkesområde, SSYK-4-grupp och yrkesbenämning.

```mermaid
graph TD;
A --narrower--> B
B --narrower--> C

    subgraph "Yrkesområde"
    A("Data/IT");
    end
    subgraph "SSYK 4"
    B("2512 - Mjukvaru- och systemuvecklare m.fl.");
    end
    subgraph "Yrkesbenämning"
    C("Backend-utvecklare");
    end
```

## Förvaltning
Yrkesområdena ägs av Arbetsförmedlingen, med enheten Jobtech som ansvarig för förvaltning av dem.

### Uppdateringsförfarande
Yrkesområdena uppdateras sällan. Det kan i sällsynta fall ske att en SSYK-4-grupp byter yrkesområde, men enbart om den ursprungliga tillhörigheten visat sig vara helt felaktig. Sådana scenarios initieras oftast av att synpunkter inkommer från användare. I de fall det finns lika god argumentation för att den ursprungliga tillhörigheten är korrekt så flyttas i regel inte SSYK-4-gruppen.   

## Begränsningar
Begränsningen med att enbart kunna koppla en SSYK-4-grupp till ett yrkesområde kan leda till att användare får svårt att hitta yrkesbenämningar som ligger i gränslandet mellan två (eller flera) yrkesområden. Se exempelvis yrkesbenämningen [CIO](http://data.jobtechdev.se/taxonomy/concept/cEY1_Bh8_URP), som genom sin koppling till SSYK-4-gruppen [1310 - IT-chefer](http://data.jobtechdev.se/taxonomy/concept/eWiB_mGP_MjE) ärver tillhörigheten till yrkesområdet [Chefer och verksamhetsledare](http://data.jobtechdev.se/taxonomy/concept/bh3H_Y3h_5eD). Även om den kopplingen är korrekt skulle yrkesbenämningen lika väl kunna anses tillhöra yrkesområdet [Data/IT](http://data.jobtechdev.se/taxonomy/concept/apaJ_2ja_LuF). För alla SSYK-4-grupper som beskriver chefsyrken gäller dock kopplingen till yrkesområdet [Chefer och verksamhetsledare](http://data.jobtechdev.se/taxonomy/concept/bh3H_Y3h_5eD).  

## Exempel på graphql-queries

Exemplen nedan kan testas via vårt [graphiql-gränssnitt](https://taxonomy.api.jobtechdev.se/v1/taxonomy/graphiql).

### Hämta hela yrkesträdet med utgångspunkt i yrkesområden

```graphql
query MyQuery {
  concepts(type: "occupation-field") {
    id
    preferred_label
    definition
    type
    narrower(type: "ssyk-level-4") {
      id
      preferred_label
      type
      ssyk_code_2012
      narrower(type: "occupation-name") {
        id
        preferred_label
        type
      }
    }
  }
}
```


