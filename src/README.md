## About
Creates one markdown file and one json file describing all concept attributes in the Jobtech Taxonomy. The attributes are mostly used in the Taxonomy GraphQL API. 

When adding or updating an attribute in the Jobtech Taxonomy database, the jobtech-taxonom-common deps will have to be updated for this project to consume the new information. 


## TODO
We should also show possible attribute-concept combinations for each version. 