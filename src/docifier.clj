(ns docifier
  (:require [jobtech-taxonomy-common.db-schema :as schema]
            [utils :as u]
            [markdown :as md]))

"Creates documentation in .json and .md
based on attribute definitions found in taxonomy-common schema"

(def definitions
  schema/definitions)

(defn kw-re
  [kw]
  (re-find #":concept(\S+)?\/(\S+)" (str kw)))

(defn concept-kw?
  [kw]
  (when-let [concept-kw (kw-re kw)]
    (last concept-kw)))

(defn parse-row
  [row]
  (when-let [concept-kw (concept-kw? (key row))]
    (merge
      ;; to handle multi-arity attribs:
      (if (vector? (first (val row)))
        {:cardinality "many"
         :type        (ffirst (val row))}
        {:cardinality "one"
         :type        (first (val row))})
      {:attribute  concept-kw
       :definition (last (val row))})))

(defn coll->map
  [coll]
  (into (sorted-map)
        (map (fn [c]
               {(:attribute c) (dissoc c :attribute)})
             (remove nil? coll))))

(defn map->json
  [path m]
  (u/to-json path m))

(defn map->md
  [path m]
  (let [md-coll (map md/m->md m)
        md-str (flatten md-coll)]
   (u/write-to-file path md-str)))

(defn -run
  []
  (let [path "attributes/attribute_definitions."
        coll (coll->map (map parse-row definitions))]
    (do (map->md (str path "md") coll)
        (map->json (str path "json") coll))))

