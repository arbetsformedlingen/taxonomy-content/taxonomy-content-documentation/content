(ns markdown
  (:require [marge.core :as marge]
            [utils :as u]))
(defn m->md
  [m]
  (let [attrib (first m)
        kvs (second  m)]
    (conj [(marge/markdown [:h1 attrib])]
          (reduce-kv
            (fn [acc k v]
              (concat acc [(marge/markdown [:h3 (u/stringify-kw k)])
                           (str (marge/markdown [:normal v]) "\n")]))
            [] kvs))))

