(ns utils
  (:require [clojure.data.json :as json]))

(defn to-json
  [path data]
  (spit path (json/write-str data {:escape-unicode false
                                   :escape-slash false
                                   :indent 4})))

(defn stringify-kw
  [s]
  (str (symbol s)))

(defn write-to-file [path lines]
  (with-open [wtr (clojure.java.io/writer path)]
    (binding [*out* wtr]
      (doseq [line lines] (print line)))))